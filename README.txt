
Content Profile Module
------------------------
by Wolfgang Ziegler, nuppla@zites.net

With this module you can build user profiles with drupal's content types.


Usage:
------
You can mark content types as user profiles, do this at 'admin/content/types'.
Then a new tab with additional settings will appear when you are editing the
content type.

